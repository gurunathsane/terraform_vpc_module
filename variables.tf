variable "name" {
  type        = string
  description = "The Name prefix for every asset created by this module"
}

variable "tags" {
  description = "The default tags attached to every asset created by this module "
  default     = {}
}

variable "vpc_cidr" {
  type        = string
  description = "(Optional) The IPv4 CIDR block for the VPC"

  validation {
    condition     = can(cidrhost(var.vpc_cidr, 0))
    error_message = "Must be valid IPv4 CIDR."
  }
}

variable "instance_tenancy" {
  type        = string
  description = "(Optional) A tenancy option for instances launched into the VPC"
  default     = "default"

  validation {
    condition     = contains(["default", "dedicated", "Standard_DS2_v2"], var.instance_tenancy)
    error_message = "Wrong input, instance_tenancy must be default or dedicated."
  }
}

variable "enable_dns_support" {
  type        = bool
  default     = true
  description = "(Optional) A boolean flag to enable/disable DNS support in the VPC. Defaults to true"
}

variable "enable_dns_hostnames" {
  type        = bool
  default     = false
  description = "(Optional) A boolean flag to enable/disable DNS hostnames in the VPC. Defaults false."
}

variable "public_subnet_cidrs" {
  type        = list(string)
  description = "List of CIDR blocks for public subnets"
}

variable "private_subnet_cidrs" {
  type        = list(string)
  description = "List of CIDR blocks for private subnets"
}

variable "nat_gateway_destination_cidr_block" {
  description = "Used to pass a custom destination route for private NAT Gateway. If not specified, the default 0.0.0.0/0 is used as a destination route."
  type        = string
  default     = "0.0.0.0/0"
}

variable "create_nat" {
  description = "Create NAT gateway? true/false"
  type        = bool
  default     = true
}

variable "launch_ha_nat" {
  default     = true
  type        = bool
  description = "Launch NAT gateway in each public zone? (default to true)"
}