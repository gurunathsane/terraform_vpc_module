data "aws_availability_zones" "current" {}

# VPC
resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr

  instance_tenancy     = var.instance_tenancy
  enable_dns_support   = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames

  tags = merge({ "Name" = "${var.name}-vpc" }, var.tags)
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = merge({ "Name" = "${var.name}-igw" }, var.tags)
}

# Public subnets
resource "aws_subnet" "public" {
  count             = length(var.public_subnet_cidrs)
  vpc_id            = aws_vpc.main.id
  cidr_block        = element(var.public_subnet_cidrs, count.index)
  availability_zone = data.aws_availability_zones.current.names[count.index]

  tags = merge({ "Name" = "${var.name}-public-subnet-${count.index}" }, var.tags)
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = merge({ "Name" = "${var.name}-route-table-public" }, var.tags)
}

resource "aws_route_table_association" "public_subnet_association" {
  count          = length(var.public_subnet_cidrs)
  subnet_id      = element(aws_subnet.public[*].id, count.index)
  route_table_id = aws_route_table.public.id
}

# Private subnet
resource "aws_subnet" "private" {
  count      = length(var.private_subnet_cidrs)
  vpc_id     = aws_vpc.main.id
  cidr_block = element(var.private_subnet_cidrs, count.index)

  availability_zone = data.aws_availability_zones.current.names[count.index]
  tags              = merge({ "Name" = "${var.name}-private-subnet-${count.index}" }, var.tags)
}

resource "aws_eip" "nat" {
  count = var.create_nat ? (var.launch_ha_nat ? length(var.public_subnet_cidrs) : 1) : 0

  vpc  = true
  tags = merge({ "Name" = "${var.name}-eip-${count.index}-${data.aws_availability_zones.current.names[count.index]}" }, var.tags)
}

resource "aws_nat_gateway" "nat" {
  count = var.create_nat ? (var.launch_ha_nat ? length(var.public_subnet_cidrs) : 1) : 0

  allocation_id = element(aws_eip.nat[*].id, count.index)
  subnet_id     = element(aws_subnet.public[*].id, count.index)

  tags = merge({ "Name" = "${var.name}-NAT-${count.index}-${data.aws_availability_zones.current.names[count.index]}" }, var.tags)

  depends_on = [aws_internet_gateway.main]
}

resource "aws_route_table" "private" {
  count = var.create_nat ? (var.launch_ha_nat ? length(var.public_subnet_cidrs) : 1) : 0

  vpc_id = aws_vpc.main.id
  tags   = merge({ "Name" = "${var.name}-route-table-private-${count.index}" }, var.tags)
}

resource "aws_route" "private_nat_gateway" {
  count = var.create_nat ? (var.launch_ha_nat ? length(var.public_subnet_cidrs) : 1) : 0

  route_table_id         = element(aws_route_table.private[*].id, count.index)
  destination_cidr_block = var.nat_gateway_destination_cidr_block
  nat_gateway_id         = element(aws_nat_gateway.nat[*].id, count.index)

  timeouts {
    create = "5m"
  }
}

resource "aws_route_table_association" "private" {
  count = var.create_nat ? (var.launch_ha_nat ? length(var.public_subnet_cidrs) : 1) : 0

  subnet_id      = element(aws_subnet.private[*].id, count.index)
  route_table_id = element(aws_route_table.private[*].id, count.index)
}