# Terraform VPC module

## Terraform Modules
Terraform modules are a way of organizing, reusing, and sharing Terraform configuration code. A module is a set of Terraform configuration files and associated resources that represent a discrete piece of infrastructure.

Modules can be used to encapsulate common infrastructure patterns, such as creating a cluster of servers or a set of networking resources. By using modules, infrastructure can be built and managed more efficiently and consistently across different environments and teams.

Terraform modules can be created by anyone and shared via the Terraform Registry, which allows users to quickly discover, use, and contribute to a growing collection of community-curated infrastructure modules.

## Terraform VPC module

This Terraform module creates a VPC with public and private subnets, NAT Gateways, and route tables in multi availability zones.

### Usage

```

module "vpc" {
  source = "gitlab.com/gurunathsane/terraform_vpc_module"

  name          = "my-vpc"
  vpc_cidr      = "10.0.0.0/16"

  public_subnet_cidrs  = [
      "10.0.1.0/24", 
      "10.0.2.0/24", 
      "10.0.3.0/24"
  ]
  private_subnet_cidrs = [
      "10.0.4.0/24", 
      "10.0.5.0/24", 
      "10.0.6.0/24"
  ]

  tags = {
    department = "devops"
  }
}

```

## Inputs

| Name | Description | Type | Default | Required |
| --- | --- | --- | --- | --- |
| name | The Name prefix for every asset created by this module | string | n/a	| yes | 
| tags | The default tags attached to every asset created by this module | map | {} | no |
| vpc_cidr | The IPv4 CIDR block for the VPC | string | n/a | yes |
| instance_tenancy | A tenancy option for instances launched into the VPC | string | default | no |
| enable_dns_support | A boolean flag to enable/disable DNS support in the VPC | boolean | true | no |
| enable_dns_hostnames | A boolean flag to enable/disable DNS hostname in the VPC | boolean | false | no |
| public_subnet_cidrs | List of CIDR blocks for public subnets | list(string) |n/a | yes |
| private_subnet_cidrs | List of CIDR blocks for private subnets | list(string) | n/a | yes |
| nat_gateway_destination_cidr_block | Used to pass a custom destination route for private NAT Gateway | string | 0.0.0.0/0 | no |
| create_nat | Create NAT gateway? true/false | boolean | true | no | 
| launch_ha_nat | Launch NAT gateway in each public zone? This is depends on value of create_nat | boolean | true | no | 

## Outputs
| Name	| Description |
| --- | --- |
| vpc_id | The ID of the VPC |
| vpc_arn | The ARN of the VPC |
| vpc_cidr_block | The CIDR block of the VPC |
| public_subnets | List of IDs of public subnets |
| public_subnet_arns | List of ARNs of public subnets | 
| public_subnets_cidr_blocks | List of cidr_blocks of public subnets |
| private_subnets | List of IDs of private subnets |
| private_subnet_arns | List of ARNs of private subnets | 
| private_subnets_cidr_blocks | List of cidr_blocks of private subnets |

